import unittest
import datetime
from dateutil import tz
from util.holidays import Holidays


class TestHolidayFunctionality(unittest.TestCase):
        
    def test_holidays_class_get_full_year(self):
        h = Holidays()
        self.assertEqual(2012, h._get_full_year(2012))
        self.assertEqual(2012, h._get_full_year(12))
        self.assertEqual(2002, h._get_full_year(2002))
        self.assertEqual(2002, h._get_full_year(2)) 
        
    def test_holidays_class_is_normal_market_hours(self):
        h = Holidays()
        opening_tests = []
        opening_tests.append([False, self.create_timestamp_EST(2012, 3, 1, 9, 29, 59, 9999)])
        opening_tests.append([True, self.create_timestamp_EST(2012, 3, 4, 9, 30, 0, 1)])
        opening_tests.append([True, self.create_timestamp_EST(2012, 3, 4, 10, 12)])
        opening_tests.append([True, self.create_timestamp_EST(2012, 4, 5, 15, 59, 59, 999)])
        opening_tests.append([False, self.create_timestamp_EST(2012, 4, 5, 16, 30, 0, 0)])
        opening_tests.append([False, self.create_timestamp_EST(2012, 4, 5, 16, 33, 21, 3)])
        
        for x in opening_tests:
            self.assertEqual(x[0], h._is_normal_market_hours(x[1]))
        
              
    #convenience
    def create_timestamp_EST(self, year, month, day, hour, minute, second=0, millisecond=0):
        return datetime.datetime(year, month, day, hour, minute, second, millisecond, tzinfo=tz.gettz('US/Eastern'))