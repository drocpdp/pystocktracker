import datetime
from dateutil import tz
import os
from base_class import BaseClass


class DateTimeOperations(BaseClass):
    
    TIME_ZONES_FILE = os.path.dirname(os.path.realpath(__file__)) + '/../config/timezones.txt'
    
    def main(self):
        self.debug(self.get_current_time_timezone('US/Eastern'))
        a = self.create_timestamp_timezone('US/Eastern', 2013,1,1,11,30)
        b = self.create_timestamp_timezone('US/Eastern', 2013,1,1,11,29,59)
        self.debug(self.is_time_past(a, b))
        self.debug(self.is_time_past(b, a))
        
    
    #convenience
    def get_current_time_EST(self):
        return self.get_current_time_timezone('US/Eastern')
        
        
    #convenience
    def create_timestamp_EST(self, year, month, day, hour, minute):
        return self.create_timestamp_timezone('US/Eastern', year, month, day, hour, minute)
    
    def get_current_time_timezone(self, timezone):
        return datetime.datetime.now(tz.gettz(timezone))
    
    def create_timestamp_timezone(self, timezone, year, month, day, hour, minute, second=0, microsecond=0):
        dt = datetime.datetime(year, month, day, hour, minute, second, microsecond, tzinfo=tz.gettz(timezone))
        return dt 
    
    
    def is_timezone_valid(self, timezone):
        tz_file = open(self.TIME_ZONES_FILE, 'r')
        for tz in tz_file:
            tz = tz.strip().split(',')
            if timezone in tz:
                return True
        tz_file.close()
        return False
    
    def is_time_past(self, time1, time2):
        if time1 > time2:
            return True
        if time1 < time2:
            return False


if __name__ == '__main__':
    DateTimeOperations().main()