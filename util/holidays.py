import csv
import os
import datetime_operations
import math
import time
import operator
from lib.emailer import Emailer
from base_class import BaseClass

class Holidays(BaseClass):
    
    HOLIDAYS_TABLE = os.path.dirname(os.path.realpath(__file__)) + '/../config/NYSE_holidays.txt'
    dt = datetime_operations.DateTimeOperations()
    normal_open_time = [9,30]
    normal_close_time = [16,00]
    century = 2000
    emailer = Emailer()

    def is_market_open_today(self):
        return self.is_market_open(self.dt.get_current_time_EST())
        
    def is_market_open(self, time_date):
        #not holiday, normal market hours = OPEN
        if not self._is_day_holiday(time_date) and self._is_normal_market_hours(time_date):
            self.debug('%s is not a holiday, and it is normal market hours. Market is OPEN' % time_date)
            return True
        #holiday, normal market hours = CLOSED
        if self._is_day_holiday(time_date) and self._is_normal_market_hours(time_date):
            self.debug('%s is a holiday. Even though these are normal market hours, Market is CLOSED.' % time_date)
            return False
        #not holiday, not normal market hours = CLOSED
        if not self._is_day_holiday(time_date) and not self._is_normal_market_hours(time_date):
            self.debug('%s is not a holiday. BUT not normal market hours, Market is CLOSED.' % time_date)
            return False            
        #holiday, not normal market hours = CLOSED
        if self._is_day_holiday(time_date) and not self._is_normal_market_hours(time_date):
            self.debug('%s is a holiday. AND also not normal market hours, Market is CLOSED.' % time_date)
            return False                    
        
    def _is_day_holiday(self, time_date):
        self.debug(time_date)
        self.debug(self._get_holidays_list())
        for holiday in self._get_holidays_list():
            if time_date >= holiday['start'] and time_date <= holiday['end']:
                self.debug('Today is holiday [%s]. [%s] is in between [%s] and [%s]' % (holiday['holiday'],time_date, holiday['start'], holiday['end']))
                return True
        self.debug('Today, [%s], is NOT a holiday' % time_date)
        return False
    
    def _is_normal_market_hours(self, time_date):
        '''
        Normally, the cron job should enforce this. Report should not be run before 9:30 and after 4:00 EST.
        This is to cover the rare edge cases where the cron job is not being run, or milliseconds differences between
        cron job execution and time of execution on python script.
        This is a bit out of scope anyways, since the report will send last trade regardless of whether or not
        time sent is off-hours.
        '''
        market_open = time_date.replace(hour=self.normal_open_time[0], minute=self.normal_open_time[1])
        market_close = time_date.replace(hour=self.normal_close_time[0], minute=self.normal_close_time[1])
        if time_date >= market_open and time_date <= market_close:
            self.debug('During [%s], Market is *normally* open (between [%s] and [%s])' % (time_date, market_open, market_close))
            return True
        else:
            self.debug('During [%s], Market is NOT *normally* open (between [%s] and [%s])' % (time_date, market_open, market_close))
            return False
        

    def _get_holidays_list(self):
        '''
        Comment
        '''
        file_obj = open(self.HOLIDAYS_TABLE, 'r')
        holiday = {}
        holidays_list = []
        for row in file_obj.readlines()[1:]:
            date_string = row.split(',')[0].strip()
            month = int(date_string.split('/')[0].strip())
            day = int(date_string.split('/')[1].strip())
            year_raw = int(date_string.split('/')[2].strip())
            year = self._get_full_year(year_raw)
            closing_time = row.split(',')[1].strip()
            holiday_string = row.split(',')[2].strip()
            if closing_time.lower() == 'close':
                start_holiday_time = self.dt.create_timestamp_EST(year, month, day, self.normal_open_time[0], self.normal_open_time[1])
            else:
                start_holiday_time = self.dt.create_timestamp_EST(year, month, day, int(closing_time.split(':')[0]), int(closing_time.split(':')[1]))
            end_holiday_time = self.dt.create_timestamp_EST(year, month, day, self.normal_close_time[0], self.normal_close_time[1])
            holiday['start'] = start_holiday_time
            holiday['end'] = end_holiday_time
            holiday['holiday'] = holiday_string
            holidays_list.append(holiday)
            holiday = {}
        file_obj.close()
        self._is_list_outdated(holidays_list)
        return holidays_list
    
    def _is_list_outdated(self, holidays_list):
        self.debug('Max = %s' % max(holidays_list))
        if self.dt.get_current_time_EST() > max(holidays_list)['start']:
            self.debug('Holiday list is outdated. Email sent to admin')
            self.emailer.admin_email('Holiday list is outdated. Most recent holiday is [%s] and today is [%s]' % (min(holidays_list)['start'], self.dt.get_current_time_EST()), 'Outdated Market Holidays List')
        else:
            self.debug('Holiday list is current')
        
    def _get_full_year(self, year_raw):
        if int(math.log10(year_raw)) + 1 <= 2:
            return self.century + year_raw
        else:
            return year_raw 

if __name__=="__main__":
    Holidays().main()