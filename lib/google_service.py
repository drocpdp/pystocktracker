import finance_service

import simplejson


class GoogleService(finance_service.FinanceService):
    
    request_url = 'http://finance.google.com/finance/info?client=ig&q=%s'
    
    key_values = {'ticker':'t',
                  'price':'l',
                  'market':'e'}
    
    def get_quote(self, ticker):
        url = self._form_request_url(ticker)
        response_content = self.get_response(url)
        json_response = self.convert_to_json(response_content)
        self.set_values(json_response)
    
    def get_code(self, code):
        return self.key_values[code]
        
    def set_values(self, json):
        self.set_ticker(json[self.get_code('ticker')])
        self.set_price(json[self.get_code('price')])
        self.set_market(json[self.get_code('market')])
        
    def _form_request_url(self, symbol):
        query = self.request_url % symbol
        return query 
        
    def convert_to_json(self, content):
        content = content.replace('[','').replace(']','')
        json_response = simplejson.loads(content[3:])
        return json_response
    