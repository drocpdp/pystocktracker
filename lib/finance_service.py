#in progress

import urllib2
import simplejson

from base_class import BaseClass


class FinanceService(BaseClass):
    
    ticker = None
    price = None
    market = None
    
    def get_response(self, url):
        request_object = urllib2.urlopen(url)
        content = request_object.read()       
        return content 
    
    def set_ticker(self, ticker):
        self.ticker = ticker
        
    def set_price(self, price):
        self.price = price
        
    def set_market(self, market):
        self.market = market
        
    def get_ticker(self):
        return self.ticker
    
    def get_price(self):
        return self.price
    
    def get_market(self):
        return self.market
    