import finance_service

class YahooService(finance_service.FinanceService):

    request_url = 'http://finance.yahoo.com/d/quotes.csv?s=%s&f=%s'
    
    key_values = {'ticker':'s',
                  'price':'l',
                  'market':'e'}
        
    def get_quote(self, ticker):
        url = self._form_request_url(ticker)
        response_content = self.get_response(url)
        csv_response = self.clean_up_content(response_content)
        self.set_values(response_content)
        
    def get_code(self, code):
        return self.key_values[code]
    
    def set_values(self, csv):
        self.set_ticker(csv.split(',')[0])
        self.set_market(csv.split(',')[1])
        self.set_price(self._convert_price(csv.split(',')[2]))
        
    def _form_request_url(self, symbol):
        query = '%s%s%s' % (self.get_code('ticker'), self.get_code('market'), self.get_code('price'))
        return self.request_url % (symbol, query)
            
    def clean_up_content(self, content):
        content = content.replace('"', '')
        return content
        
    def _convert_price(self, price_formatted):
        self.price = float(price_formatted.split('<b>')[1].split('</b>')[0])
        
    def _set_market(self, content):
        self.market = content
    