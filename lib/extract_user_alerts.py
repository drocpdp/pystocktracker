#TODO: Loop through a directory

import xlrd
import ConfigParser
import os
from base_class import BaseClass

class ExtractUserAlerts(BaseClass):
    
    PROPERTIES_FILE = os.path.dirname(os.path.realpath(__file__)) + '/../config/pystocktracker.properties'
    
    rows_schema = {'ticker':0,
                   'price':1,
                   'comparison':2}
    
    def get_user_alerts(self):
        self.debug(self.get_alerts_location())
        wb = xlrd.open_workbook(self.get_alerts_location())
        all_orders = []
        for sheet in wb.sheets():
            self.debug('Sheet:%s' % sheet.name)
            for row in range(sheet.nrows):
                if row == 0:
                    continue
                alert_row = {}
                alert_row['ticker'] = sheet.cell(row,self.rows_schema['ticker']).value
                alert_row['price'] = sheet.cell(row,self.rows_schema['price']).value
                alert_row['comparison'] = sheet.cell(row,self.rows_schema['comparison']).value
                self.debug(alert_row)
                all_orders.append(alert_row)
        self.debug(all_orders)
        return all_orders

    def get_alerts_location(self):
        self.debug(self.PROPERTIES_FILE)
        return self.get_property(self.PROPERTIES_FILE, 'default', 'alerts_sheet_file')
    
    def get_property(self, property_file, property_section, property_key):
        self.debug(property_file)
        property = ConfigParser.RawConfigParser()
        property.read(property_file)
        return property.get(property_section, property_key)
        