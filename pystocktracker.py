 #http://finance.google.com/finance/info?client=ig&q=AFSI,C

import time
import urllib2
import simplejson
import os

from base_class import BaseClass
from util.holidays import Holidays
from lib.extract_user_alerts import ExtractUserAlerts
from lib.emailer import Emailer
from lib.google_service import GoogleService
from lib.yahoo_service import YahooService

class PyStockTracker(BaseClass):

    def main(self):
        
        goog = GoogleService()
        yahoo = YahooService()
        
        holidays = Holidays()
        
        service_object = goog
        
        ua = ExtractUserAlerts()
        user_set_alerts = ua.get_user_alerts()
        
        result = ''
        self.debug('OK HERE')
        if not holidays.is_market_open_today():
            
            for user_alert in user_set_alerts:
                self.debug(user_alert)
                service_object.get_quote(user_alert['ticker'])
                user_price = user_alert['price']
                symbol = service_object.get_ticker()
                market = service_object.get_market()
                actual_last_price = service_object.get_price()
                self.debug(symbol)
                self.debug(market)
                self.debug(actual_last_price)
                
                self.debug('User price = %s ---- actual last price = %s' % (user_price, actual_last_price))
                if user_alert['comparison'] == 'GT':
                    if actual_last_price > user_price:
                        result += 'ALERT: Symbol: %s, in %s, Market price of %s is GREATER THAN watch price of %s\n' % (symbol, market, actual_last_price, user_price)
                        self.debug('ALERT: Symbol: %s, in %s, Market price of %s is GREATER THAN watch price of %s' % (symbol, market, actual_last_price, user_price))
                    else:
                        self.debug('GT -- DO NOT SEND ALERT %s !> %s' % (actual_last_price, user_price))
                if user_alert['comparison'] == 'LT':
                    if actual_last_price < user_price:
                        result += 'ALERT: Symbol: %s, in %s, Market price of %s is LESS THAN watch price of %s\n' % (symbol, market, actual_last_price, user_price)
                        self.debug('ALERT: Symbol: %s, in %s, Market price of %s is LESS THAN watch price of %s' % (symbol, market, actual_last_price, user_price))
                    else:
                        self.debug('LT --DO NOT SEND ALERT %s !< %s' % (actual_last_price, user_price))
                if user_alert['comparison'] == 'EQ':
                    if actual_last_price == user_price:
                        result += 'ALERT: Symbol: %s, in %s, Market price of %s is EQUAL TO watch price of %s\n' % (symbol, market, actual_last_price, user_price)
                        self.debug('ALERT: Symbol: %s, in %s, Market price of %s is EQUAL watch price of %s' % (symbol, market, actual_last_price, user_price))    
                    else:
                        self.debug('EQ -- DO NOT SEND ALERT %s !== %s' % (actual_last_price, user_price))       
                        
            emailer = Emailer()
            emailer.email(result)                       

    
if __name__ == '__main__':
    PyStockTracker().main()